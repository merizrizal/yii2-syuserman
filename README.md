Synctech User Manager
=====================
Synctech User Manager

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist synctech/yii2-syuserman "*"
```

or add

```
"synctech/yii2-syuserman": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \syuserman\AutoloadExample::widget(); ?>```
