<?php

namespace syuserman\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use syuserman\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_level_id', 'not_active'], 'integer'],
            [['email', 'nama_user', 'image', 'password', 'created_at', 'updated_at',
                'userLevel.nama_level'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['userLevel.nama_level']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['userLevel']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);
        
        $dataProvider->sort->attributes['userLevel.nama_level'] = [
            'asc' => ['user_level.nama_level' => SORT_ASC],
            'desc' => ['user_level.nama_level' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.user_level_id' => $this->user_level_id,
            'user.not_active' => $this->not_active,
            'user.created_at' => $this->created_at,
            'user.updated_at' => $this->updated_at,
        ]);
        
        $query->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'user.nama_user', $this->nama_user])
            ->andFilterWhere(['like', 'user.image', $this->image])
            ->andFilterWhere(['like', 'user.password', $this->password])
            ->andFilterWhere(['like', 'user_level.nama_level', $this->getAttribute('userLevel.nama_level')]);

        return $dataProvider;
    }
}
