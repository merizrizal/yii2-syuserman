<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use syuserman\models\UserLevel;
use sycomponent\AjaxRequest;
use sycomponent\NotificationDialog;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */

kartik\select2\Select2Asset::register($this);
kartik\select2\ThemeKrajeeAsset::register($this);

$ajaxRequest = new AjaxRequest([
    'modelClass' => 'User',
]);

$ajaxRequest->form();

$status = Yii::$app->session->getFlash('status');
$message1 = Yii::$app->session->getFlash('message1');
$message2 = Yii::$app->session->getFlash('message2');

if ($status !== null) : 
    $notif = new NotificationDialog([
        'status' => $status,
        'message1' => $message1,
        'message2' => $message2,
    ]);

    $notif->theScript();
    echo $notif->renderDialog();

endif;

$this->title = 'Update User Password: ' . ' ' . $model->nama_user;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_user, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update User Password'; ?>

<?= $ajaxRequest->component() ?>

<div class="user-update">

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="x_panel">            
                <div class="user-form">

                    <?php $form = ActiveForm::begin([
                            'id' => 'user-form',
                            'action' => ['update-password', 'id' => $model->id],
                            'options' => [

                            ],
                            'fieldConfig' => [
                                'parts' => [
                                    '{inputClass}' => 'col-lg-12'
                                ],
                                'template' => '<div class="row">'
                                                . '<div class="col-lg-3">'
                                                    . '{label}'
                                                . '</div>'
                                                . '<div class="col-lg-6">'
                                                    . '<div class="{inputClass}">'
                                                        . '{input}'
                                                    . '</div>'
                                                . '</div>'
                                                . '<div class="col-lg-3">'
                                                    . '{error}'
                                                . '</div>'
                                            . '</div>', 
                            ]
                    ]); ?>
                    
                        <div class="x_title">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <?php
                                        if (!$model->isNewRecord)
                                            echo Html::a('<i class="fa fa-upload"></i>&nbsp;&nbsp;&nbsp;' . 'Create', ['create'], ['class' => 'btn btn-success']); ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    
                        <div class="x_content">

                            <?= $form->field($model, 'id', [
                                        'parts' => [
                                            '{inputClass}' => 'col-lg-7'
                                        ],
                                        'enableAjaxValidation' => true
                                    ])->textInput(['maxlength' => 32, 'readonly' => 'readonly']) ?>

                            <?= $form->field($model, 'user_level_id')->dropDownList(
                                    ArrayHelper::map(
                                        UserLevel::find()->orderBy('nama_level')->asArray()->all(), 
                                        'id', 
                                        function($data) { 
                                            return $data['nama_level'];                                 
                                        }
                                    ), 
                                    [
                                        'prompt' => '',
                                        'style' => 'width: 80%'
                                    ]) ?>

                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 64, 'value' => '']) ?>                        

                            <?php
                            if ($model->isNewRecord)
                                echo $form->field($model, 'password')->passwordInput(['maxlength' => 64]); ?>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6">
                                        <?php
                                        $icon = '<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;&nbsp;';
                                        echo Html::submitButton($model->isNewRecord ? $icon . 'Save' : $icon . 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                                        echo '&nbsp;&nbsp;&nbsp;';
                                        echo Html::a('<i class="fa fa-rotate-left"></i>&nbsp;&nbsp;&nbsp;Cancel', ['index'], ['class' => 'btn btn-default']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div><!-- /.row -->

</div>

<?php
$this->registerCssFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/all.css', ['depends' => 'backend\assets\AppAsset']);
 
$this->registerJsFile($this->params['assetCommon']->baseUrl . '/plugins/iCheck/icheck.min.js', ['depends' => 'backend\assets\AppAsset']);

$jscript = '
    $("#user-user_level_id").select2({
        theme: "krajee",
        placeholder: "Pilih",
        allowClear: true
    });
    
    $("#user-user_level_id").prop("disabled", true);
';

$this->registerJs($jscript); ?>
